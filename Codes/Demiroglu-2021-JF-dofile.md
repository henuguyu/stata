> Demiroglu, C. E. M., O. Ozbas, R. C. Silva, M. F. Ulu, **2021**, 
Do physiological and spiritual factors affect economic decisions?, **The Journal of Finance**, 76 (5): 2481-2523. 
[-Link-](https://doi.org/10.1111/jofi.13032), [-PDF1-](https://sci-hub.ren/10.1111/jofi.13032), 
[-PDF-](https://onlinelibrary.wiley.com/doi/epdf/10.1111/jofi.13032), 
[-Appendix-](https://onlinelibrary.wiley.com/action/downloadSupplement?doi=10.1111%2Fjofi.13032&file=jofi13032-sup-0001-InternetAppendix.pdf), 
[-Replication-](https://onlinelibrary.wiley.com/action/downloadSupplement?doi=10.1111%2Fjofi.13032&file=jofi13032-sup-0002-ReplicationCode.zip)

```stata

/********************************************************************************
 INTRODUCTION
 
 THIS CODE REPRODUCES ALL THE RESULTS EXCEPT TABLE 8 IN 
 "Do Physiological and Spiritual Factors Affect Economic Decisions?" IN THE  
 JOURNAL OF FINANCE.

 PLEASE REFER TO THE DATA SECTION OF THE PAPER FOR A DETAILED DISCUSSION IF DATA
 DESCRIPTION AND SOURCES.
 
 ALL OF THE TABLES USE THE SAME LOAN LEVEL DATASET EXCEPT TABLE 8 WHERE WE USE 
 BRANCH-DAY LEVEL DATA FOR THE ANALYSIS.
 
********************************************************************************/
  
 
set more off
/*********************************************
	Global directories and data
*********************************************/

global dir_Ddta "D:"
global dir_log  "D:/TablesFinal"

set matsize 10000

********************************************************************************

/*
label var maturity_m "Original loan maturity (in months)"
label var l_r_principal "LN(Loan amount (000 TL, in 2008 prices))"
label var fx_dum "Foreign currency loan (d)"
label var SECUREDfrac "Collateral / Loan amount"
label var RLoanrate "Loan rate (%, adjusted for CPI)"
label var ZeroLoanrate "Loan rate is zero (d)"
label var riskweight "Regulatory risk weight of the loan"
*/


global loanchar "maturity_m l_r_principal fx_dum SECUREDfrac  RLoanrate ZeroLoanrate riskweight "  /*We add i.rating to regression specs below*/
global lunar " Muharram Safar Rabialawwal Rabialthanny Jamadaalawwal Jamadaalthanny Rajab Shaban Shawwal Dhulqadah Dhulhijjah"
global Macrovars "Lvix_eop "
global smallbiz "smallbiz==1 & firmCR_dum==1 & statebank==0"


***************************************************
* TABLE 1. SUMMARY STATISTICS FOR SMALL BIZ LOANS;
***************************************************
 
capture program drop Table1
program define Table1

#delimit ;
local sumvars "NPL2 Ramadan100  RLoanrate ZeroLoanrate100 loanspread  riskweight100 marginal_riskweight l_r_principal 
SECUREDfrac100 maturity_m  fx_dum100 rating  freshborrower100 old_customer_branch100 l_noofdailynewloans
smallcity100  BankCAR100 LBliq_rat100 l_noofborrowers summer100 H3 $Macrovars adha_loan100
secured100 unsecured100 undersecured100" ;
  
#delimit cr  

qui: sum `sumvars' if $smallbiz

qui: estpost tabstat `sumvars' if $smallbiz, statistics( count mean sd ) columns(statistics)
estimates store Table1_1
qui: estpost tabstat `sumvars' if Ramadan==1 & $smallbiz, statistics( count mean sd ) columns(statistics)
estimates store Table1_2
qui: estpost tabstat `sumvars' if Ramadan==0 & $smallbiz, statistics( count mean sd  ) columns(statistics)
estimates store Table1_3


esttab Table1_*, cells("mean(fmt(2)) sd(fmt(2))") collabels("Mean" "SD")  ///
mtitles("Full Sample" "Ramadan" "Non-Ramadan" "Difference") varwidth(35) modelwidth(20) ///
label level(95) nonumber  title("Table 1. Summary Statistics") 


* Count the number of unique Loans, bank branches abd borrowers

count if $smallbiz
distinct bankbranch_fe borrower_fe if $smallbiz

count if Ramadan==0 & $smallbiz
distinct bankbranch_fe borrower_fe if Ramadan==0 & $smallbiz

count if Ramadan==1 & $smallbiz
distinct bankbranch_fe borrower_fe if Ramadan==1 & $smallbiz

end 


*************************************************************************************
* Table II. The Effect of Ramadan on the Likelihood of Default: Small Business Loans
*************************************************************************************
capture program drop Table2
program define Table2


qui:qui: reghdfe NPL2 Ramadan $Macrovars   if $smallbiz , absorb(openingyear openingmonth ) vce(cluster  openingym) keepsing
est store Table2_1

qui: reghdfe NPL2 Ramadan $Macrovars  $loanchar i.rating  if $smallbiz , absorb(openingyear openingmonth activity ) vce(cluster  openingym) keepsing
est store Table2_2

qui: reghdfe NPL2 Ramadan $Macrovars  $loanchar i.rating  if $smallbiz , absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster  openingym) keepsing
est store Table2_3

qui: reghdfe NPL2 Ramadan $Macrovars  $loanchar i.rating  if $smallbiz , absorb(openingyear              activity bankbranchmonth_fe ) vce(cluster  openingym) keepsing
est store Table2_4

qui: reghdfe NPL2 Ramadan $Macrovars  $loanchar i.rating  if $smallbiz , absorb(             openingmonth activity bankbranchyear_fe ) vce(cluster  openingym) keepsing
est store Table2_5

qui: reghdfe NPL2 Ramadan $Macrovars  $loanchar i.rating  if $smallbiz , absorb(openingyear openingmonth activity bankbranch_fe borrower_fe)  vce(cluster  openingym) keepsing
est store Table2_6

qui: reghdfe NPL2 Ramadan $Macrovars  $loanchar i.rating  if $smallbiz & span==1, absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster  openingym) keepsing
est store Table2_7

estfe Table2_*, labels(openingyear "Year FEs" openingmonth "Month FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs" borrower_fe "Borrower FEs" bankbranchmonth_fe "Branch x Month FEs" bankbranchyear_fe "Branch x Year FEs"  bankbranchborrower_fe "Branch x Borrower FEs")

esttab Table2_*, cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(10)  nomtitles level(95)  collabels(none) nodepvars  keep(Ramadan ) ///
indicate("Borrower/loan characteristics =   $loanchar" "Macro control =   $Macrovars" `r(indicate_fe)') ///
stats(N r2, fmt(0 3) labels(`"N"' `"R-squared"')) ///
title ("Table II. The Effect of Ramadan on the Likelihood of Default: Small Business Loans") ///
addnotes("Standard errors that are heteroskedasticity-consistent and clustered at the month-year level are reported in parentheses beneath coefficient estimates.")

end 


*************************************************************************************
*Table III. The Effect of Ramadan on the Likelihood of Default by Calendar Year
*************************************************************************************


capture program drop Table3
program define Table3


*By Calendar year
foreach y of numlist 2003/2013{
qui:reghdfe NPL2 Ramadan  $Macrovars  $loanchar i.rating if  openingyear==`y' & $smallbiz ,abs( bankbranch_fe activity) vce(robust) 
est store Table3_`y'
}


estfe Table3_*, labels(openingmonth "Month FEs" openingyear "Year FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs")

esttab Table3_*, cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01) ///
varwidth(30) modelwidth(10)  nomtitles level(95)  collabels(none) nodepvars  keep(Ramadan ) ///
stats(N r2, fmt(0 3) labels(`"N"' `"R-squared"'))     title("Table III. The Effect of Ramadan on the Likelihood of Default by Calendar Year") ///
indicate("Borrower/loan characteristics =   $loanchar" "Macro control =   $Macrovars" `r(indicate_fe)') ///
mgroups("2003" "2004" "2005" "2006" "2007" "2008" "2009" "2010" "2011" "2012" "2013" , pattern(1 1 1 1 1 1 1 1 1 1 1)) ///
addnotes("Standard errors that are heteroskedasticity-consistent are reported in parentheses beneath coefficient estimates.")


end


*************************************************************************************
*Table IV. Yearly Default Models with Calendar Month Fixed Effects
*************************************************************************************

capture program drop Table4
program define Table4



*By Calendar year
foreach y of numlist 2003/2013{
qui:reghdfe NPL2 month1-month12 $loanchar $Macrovars  i.rating if  openingyear==`y' & $smallbiz ,abs( bankbranch_fe activity) vce(robust) 
est store Table4_`y'

}

estfe Table4_*, labels(openingmonth "Month FEs" openingyear "Year FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs")

esttab Table4_*, cells(b(star fmt(3)) se(fmt(3) par abs) ) label star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(10)  nomtitles level(95)  collabels(none) nodepvars  keep(month*) ///
indicate("Borrower/loan characteristics =   $loanchar" "Macro control =   $Macrovars" `r(indicate_fe)') ///
stats(N r2, fmt(0 3) labels(`"N"' `"R-squared"')) ///
title ("Table IV. Yearly Default Models with Calendar Month Fixed Effect") ///
mgroups("2003" "2004" "2005" "2006" "2007" "2008" "2009" "2010" "2011" "2012" "2013" , pattern(1 1 1 1 1 1 1 1 1 1 1)) ///
addnotes("Standard errors that are heteroskedasticity-consistent are reported in parentheses beneath coefficient estimates.")



end




*************************************************************************************
*Table V. The Effect of Ramadan on the Likelihood of Default Excluding 2007 and 2011
*************************************************************************************

capture program drop Table5
program define Table5

qui: reghdfe NPL2 Ramadan $Macrovars  $loanchar i.rating  if $smallbiz & openingyear!=2007 & openingyear!=2011 , absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster  openingym) keepsing
est store Table5_1

estfe Table5_*, labels(openingyear "Year FEs" openingmonth "Month FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs")

esttab Table5_*, cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(10)  nomtitles level(95)  collabels(none) nodepvars  keep(Ramadan ) ///
indicate("Borrower/loan characteristics =   $loanchar" "Macro control =   $Macrovars" `r(indicate_fe)') ///
stats(N r2, fmt(0 3) labels(`"N"' `"R-squared"')) ///
title ("Table V. The Effect of Ramadan on the Likelihood of Default Excluding 2007 and 2011") ///
addnotes("Standard errors that are heteroskedasticity-consistent and clustered at the month-year level are reported in parentheses beneath coefficient estimates.")


end


*************************************************************************************
*Table VI. Ramadan Loans and Likelihood of Default by Loan Type
*************************************************************************************

capture program drop Table6
program define Table6


qui: reghdfe NPL2 Ramadan  $Macrovars $loanchar i.rating if medbiz==1 & firmCR_dum==1 & statebank==0, absorb(openingyear openingmonth activity bankbranch_fe) vce(cluster  openingym) keepsing
est store Table6_1

qui: reghdfe NPL2 Ramadan  $Macrovars $loanchar i.rating if bigbiz==1 & firmCR_dum==1 & statebank==0, absorb(openingyear openingmonth activity bankbranch_fe) vce(cluster  openingym) keepsing
est store Table6_2

qui: reghdfe NPL2 Ramadan  $Macrovars $loanchar i.rating if activity==2 & firmCR_dum==0 & statebank==0, absorb(openingyear openingmonth activity bankbranch_fe) vce(cluster  openingym) keepsing
est store Table6_3

qui: reghdfe NPL2 Ramadan  $Macrovars $loanchar i.rating if activity==3 & firmCR_dum==0 & statebank==0, absorb(openingyear openingmonth activity bankbranch_fe) vce(cluster  openingym) keepsing
est store Table6_4


estfe Table6_*, labels(openingyear "Year FEs" openingmonth "Month FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs")


esttab Table6_* , cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(10) label nomtitles level(95)  collabels(none) nodepvars  keep(Ramadan) ///
indicate("Borrower/loan characteristics =   $loanchar" "Macro control =   $Macrovars" `r(indicate_fe)') ///
stats(N r2, fmt(0 2) labels(`"N"' `"R-squared"')) ///
title ("Table VI. Ramadan Loans and Likelihood of Default by Loan Type") ///
mgroups( "Medium" "Large" "Mortgage" "Auto" , pattern(1 1 1 1 ))  ///
addnotes("Standard errors that are heteroskedasticity-consistent and clustered at the month-year level are reported in parentheses beneath coefficient estimates.")


end


*************************************************************************************
*Table VII. The Effect of Ramadan on Credit Spreads and Collateral
*************************************************************************************


capture program drop Table7
program define Table7

global spreadcontrols=" ZeroLoanrate riskweight SECUREDfrac maturity_m l_r_principal"
global securedcontrols=" ZeroLoanrate RLoanrate maturity_m l_r_principal fx_dum"
global sizecontrols=" ZeroLoanrate RLoanrate SECUREDfrac maturity_m fx_dum"
global commoncontrols="ZeroLoanrate riskweight maturity_m"

*Loanspread

qui: reghdfe loanspread Ramadan $Macrovars if $smallbiz & loanspread>0 & fx_dum==0, absorb(openingyear openingmonth activity bankbranch_fe) vce(cluster  openingym) keepsing
est store Table7_1

qui: reghdfe loanspread Ramadan $Macrovars $spreadcontrols i.rating if $smallbiz & loanspread>0 & fx_dum==0, absorb(openingyear openingmonth activity bankbranch_fe) vce(cluster  openingym) keepsing
est store Table7_2

*Collateral/ Loan amount

qui: reghdfe SECUREDfrac Ramadan $Macrovars if $smallbiz, absorb(openingyear openingmonth activity bankbranch_fe) vce(cluster  openingym) keepsing
est store Table7_3

qui: reghdfe SECUREDfrac Ramadan $Macrovars $securedcontrols i.rating if $smallbiz, absorb(openingyear openingmonth activity bankbranch_fe) vce(cluster  openingym) keepsing
est store Table7_4

*Pr(Collateral>0)

qui: reghdfe secured Ramadan $Macrovars if $smallbiz, absorb(openingyear openingmonth activity bankbranch_fe) vce(cluster  openingym) keepsing
est store Table7_5

qui: reghdfe secured Ramadan $Macrovars $securedcontrols i.rating if $smallbiz, absorb(openingyear openingmonth activity bankbranch_fe) vce(cluster  openingym) keepsing
est store Table7_6

estfe Table7_*, labels(openingyear "Year FEs" openingmonth "Month FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs")

esttab Table7_* , cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(18) label nomtitles level(95)  collabels(none) nodepvars  keep(Ramadan) /// 
indicate("Borrower/loan characteristics = $commoncontrols"  "Macro control =   $Macrovars" `r(indicate_fe)') ///
stats(N r2, fmt(0 2) labels(`"N"' `"R-squared"')) ///
title ("Table VII. The Effect of Ramadan on Credit Spreads and Collateral") ///
mgroups( "Loanspread" "Collateral (%)" "Secured (d)", pattern(1 0 1 0 1 0)) ///
addnotes("Standard errors that are heteroskedasticity-consistent and clustered at the month-year level are reported in parentheses beneath coefficient estimates.")



end





*************************************************************************************
*Table VIII. Branch-Level Daily Loan Volume and the Difficulty of Fasting
*************************************************************************************


capture program drop Table8
program define Table8

global avloanchar "RINT MAT RW SEC LVAL "


qui: reghdfe l_noofdailynewloans   Ramadan     $avloanchar if tag_branch_opdate==1, vce(cluster  openingym) abs( openingyear  bankbranch_fe)
est store Table8_1

capture drop intensity Ramadan_intensity
g intensity=H3
g Ramadan_intensity=Ramadan*H3
qui: reghdfe l_noofdailynewloans  Ramadan_intensity Ramadan  intensity     $avloanchar if tag_branch_opdate==1, vce(cluster  openingym) abs(  openingyear  bankbranch_fe)
est store Table8_2

capture drop intensity Ramadan_intensity
g intensity=summer
g Ramadan_intensity=Ramadan*summer
qui: reghdfe l_noofdailynewloans Ramadan_intensity Ramadan  intensity   $avloanchar if tag_branch_opdate==1, vce(cluster  openingym) abs(  openingyear bankbranch_fe)
est store Table8_3


label var intensity "Intensity"
label var Ramadan_intensity "Ramadan*Intensity"

estfe Table8_*, labels(openinyear "Year FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs")


esttab Table8_* , cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(17) label nomtitles level(95)  collabels(none) nodepvars  keep(Ramadan  Ramadan_intensity intensity) /// 
indicate("Borrower/loan characteristics = $avloanchar"  `r(indicate_fe)') ///
stats(N r2, fmt(0 2) labels(`"N"' `"R-squared"')) ///
title ("Table VIII. Branch-Level Daily Loan Volume and the Difficulty of Fasting") ///
mgroups( "" "Local Temperature" "Summer", pattern(1  1  1 )) ///
addnotes("Standard errors that are heteroskedasticity-consistent and clustered at the month-year level are reported in parentheses beneath coefficient estimates.")


end



*************************************************************************************
*Table IX. The Effect of Ramadan on Loan Performance by Branch Busyness
*************************************************************************************

capture program drop Table9
program define Table9

*** By Busynesss quartile 
qui: reghdfe NPL2 Ramadan   $Macrovars $loanchar i.rating if $smallbiz & busyBr3==1 , absorb(openingmonth openingyear activity bankbranch_fe ) vce(cluster  openingym) keepsing
est store Table9_1
 
qui: reghdfe NPL2 Ramadan   $Macrovars $loanchar i.rating if $smallbiz & busyBr3==2 , absorb(openingmonth openingyear activity bankbranch_fe) vce(cluster  openingym) keepsing
est store Table9_2

qui: reghdfe NPL2 Ramadan   $Macrovars $loanchar i.rating if $smallbiz & busyBr3==3 , absorb(openingmonth openingyear activity bankbranch_fe ) vce(cluster  openingym) keepsing
est store Table9_3

qui: reghdfe NPL2 Ramadan   $Macrovars $loanchar i.rating if $smallbiz & busyBr3==4 , absorb(openingmonth openingyear activity bankbranch_fe ) vce(cluster  openingym) keepsing
est store Table9_4

 
estfe Table9_*, labels(openingyear "Year FEs" openingmonth "Month FEs"  activity "Loan purpose FEs" bankbranch_fe "Branch FEs")

esttab Table9_* ,cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(20) label nomtitles level(95)  collabels(none) nodepvars keep(Ramadan)  /// /*legend */
indicate("Borrower/loan characteristics = $loanchar" "Macro control = $Macrovars"  `r(indicate_fe)') ///
stats(N r2, fmt(0 2) labels(`"N"' `"R-squared"')) ///
title (" Table IX. The Effect of Ramadan on Loan Performance by Branch Busyness")  ///
mgroups(  "Q1, Lowest Busyness" "Q2" "Q3" "Q4, Top Busyness"  , pattern(   1  1  1  1  )) ///
addnotes("Standard errors that are heteroskedasticity-consistent and clustered at the month-year level are reported in parentheses beneath coefficient estimates.")


end


*************************************************************************************
*Table X. Loan Default and Difficulty of Fasting at Origination
*************************************************************************************

capture program drop Table10
program define Table10

capture drop busyness 
capture drop ramadan_busyness

*Temperature

qui: g intensity = H3_dm
qui: g Ramadan_intensity = Ramadan * intensity
qui: g busyness = l_noofdailynewloans_dm
qui: g intensity_busyness = intensity * busyness
qui: g ramadan_busyness = Ramadan * busyness
qui: g Ramadan_intensity_busyness = Ramadan * intensity * busyness


qui: reghdfe NPL2 Ramadan intensity Ramadan_intensity busyness intensity_busyness ramadan_busyness Ramadan_intensity_busyness  $Macrovars $loanchar i.rating if $smallbiz , absorb( openingyear activity bankbranch_fe ) vce(cluster  openingym) keepsing
est store Table10_1

qui: reghdfe NPL2 Ramadan intensity Ramadan_intensity  $Macrovars $loanchar i.rating if $smallbiz & busyBr3==4, absorb( openingyear activity bankbranch_fe ) vce(cluster  openingym) keepsing
est store Table10_2
 
drop intensity Ramadan_intensity intensity_busyness Ramadan_intensity_busyness


*Summer

qui: g intensity = summer
qui: g Ramadan_intensity = Ramadan * intensity
qui: g intensity_busyness = intensity * busyness
qui: g Ramadan_intensity_busyness = Ramadan * intensity * busyness


qui: reghdfe NPL2 Ramadan intensity Ramadan_intensity busyness intensity_busyness ramadan_busyness Ramadan_intensity_busyness  $Macrovars $loanchar i.rating if $smallbiz , absorb( openingyear activity bankbranch_fe ) vce(cluster  openingym) keepsing
est store Table10_3

qui: reghdfe NPL2 Ramadan intensity Ramadan_intensity  $Macrovars $loanchar i.rating if $smallbiz & busyBr3==4, absorb( openingyear activity bankbranch_fe) vce(cluster  openingym) keepsing
est store Table10_4

label variable intensity "Intensity"
label variable Ramadan_intensity "Ramadan x Intensity"
label variable busyness "Branch busyness"
label variable intensity_busyness "Intensity x Branch busyness"
label variable ramadan_busyness "Ramadan x Branch busyness"
label variable Ramadan_intensity_busyness "Ramadan x Intensity x Branch busyness"


estfe Table10_*, labels(openingmonth "Month FEs" openingyear "Year FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs")

esttab Table10_* ,cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(10) label nomtitles level(95)  collabels(none) nodepvars  ///
indicate("Borrower/loan characteristics =   $loanchar" "Macro control =   $Macrovars" `r(indicate_fe)') ///
stats(N r2, fmt(0 2) labels(`"N"' `"R-squared"'))  ///
keep(Ramadan intensity Ramadan_intensity  busyness intensity_busyness ramadan_busyness  Ramadan_intensity_busyness busyness) ///
title ("Table X. Loan Default and Difficulty of Fasting at Origination")  ///
mgroups("Temp."  "Summer" , pattern(1 0 1 0 ) ) ///
addnotes("Standard errors that are heteroskedasticity-consistent and clustered at the month-year level are reported in parentheses beneath coefficient estimates.")


drop intensity Ramadan_intensity ramadan_busyness intensity_busyness Ramadan_intensity_busyness busyness

end



*************************************************************************************
*Table XI. Charitable Lending in Ramadan
*************************************************************************************

capture program drop Table11
program define Table11


* Old customer

qui: g charitable = old_customer_branch
qui: g Ramadan_charitable  = Ramadan * charitable

qui: reghdfe NPL2 Ramadan Ramadan_charitable charitable  $Macrovars $loanchar i.rating if $smallbiz, absorb(openingmonth openingyear activity bankbranch_fe ) vce(cluster  openingym) keepsing
estimates store Table11_1

drop charitable Ramadan_charitable

*Bank Liquidity

qui: g charitable = LBliq_rat
qui: g Ramadan_charitable  = Ramadan * charitable

qui: reghdfe NPL2 Ramadan Ramadan_charitable charitable  $Macrovars $loanchar i.rating if $smallbiz , absorb(openingmonth openingyear activity bankbranch_fe) vce(cluster  openingym) keepsing
estimates store Table11_2

drop charitable Ramadan_charitable

*Capital Adequacy Ratio

qui: g charitable = BankCAR
qui: g Ramadan_charitable  = Ramadan * charitable

qui: reghdfe NPL2 Ramadan Ramadan_charitable charitable  $Macrovars $loanchar i.rating if $smallbiz, absorb(openingmonth openingyear activity bankbranch_fe ) vce(cluster  openingym) keepsing
estimates store Table11_3

qui: reghdfe NPL2 Ramadan Ramadan_charitable charitable  $Macrovars $loanchar i.rating if $smallbiz & islamic==0, absorb(openingmonth openingyear activity bankbranch_fe ) vce(cluster  openingym) keepsing
estimates store Table11_4

qui: reghdfe NPL2 Ramadan Ramadan_charitable charitable  $Macrovars $loanchar i.rating if $smallbiz & secured==1, absorb(openingmonth openingyear activity bankbranch_fe ) vce(cluster  openingym) keepsing
estimates store Table11_5


label variable charitable "Charitable"
label variable Ramadan_charitable "Ramadan x Charitable"

estfe Table11_*, labels(openingyear "Year FEs" openingmonth "Month FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs")

esttab Table11_*, cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(18) label nomtitles level(95)  collabels(none) nodepvars  keep(Ramadan Ramadan_charitable charitable) ///
indicate("Borrower/loan characteristics =   $loanchar" "Macro control =   $Macrovars" `r(indicate_fe)') ///
stats(N r2, fmt(0 2) labels(`"N"' `"R-squared"')) title ("Table XI. Charitable Lending in Ramadan")  ///
mgroups("Rating" "Collateral." "Old client" "Bank capital" "Bank liquidity" "Loan Risk weight" "I(Riskweight>1)", pattern(1 1 1 1 1 1 1)) ///
addnotes("Standard errors that are heteroskedasticity-consistent and clustered at the month-year level are reported in parentheses beneath coefficient estimates.")


drop charitable Ramadan_charitable

end


*************************************************************************************
*Table XII. Alternative Mechanisms
*************************************************************************************

capture program drop Table12
program define Table12

capture drop charitable Ramadan_charitable
qui: g charitable = l_noofborrowers
qui: g Ramadan_charitable  = Ramadan * charitable
qui: reghdfe NPL2 Ramadan Ramadan_charitable charitable  $Macrovars $loanchar  if $smallbiz  , absorb(openingyear openingmonth activity bankbranch_fe) vce(cluster  openingym) keepsing
estimates store Table12_1	


capture drop charitable Ramadan_charitable
qui: g charitable = smallcity
qui: g Ramadan_charitable  = Ramadan * charitable
qui: reghdfe NPL2 Ramadan Ramadan_charitable    $Macrovars $loanchar i.rating if $smallbiz , absorb(openingyear openingmonth activity bankbranch_fe) vce(cluster  openingym) keepsing
est store Table12_2
	

capture drop charitable Ramadan_charitable
qui: g charitable = freshborrower
qui: g Ramadan_charitable  = Ramadan * charitable	
qui: reghdfe NPL2  Ramadan Ramadan_charitable charitable   $Macrovars $loanchar i.rating  if $smallbiz & openingym>=tm(2005m1), absorb(openingmonth openingyear activity bankbranch_fe ) vce(cluster  openingym) keepsing
estimates store Table12_3

	
 *Firm rating 
 
local loanchar_rating="riskweight RLoanrate ZeroLoanrate SECUREDfrac maturity_m l_r_principal fx_dum "

capture drop charitable Ramadan_charitable
qui: g charitable = rating
qui: g Ramadan_charitable  = Ramadan * charitable
 
qui: reghdfe NPL2 Ramadan Ramadan_charitable charitable  $Macrovars `loanchar_rating' if rating!=0 & $smallbiz, absorb(openingmonth openingyear activity bankbranch_fe ) vce(cluster  openingym) keepsing
estimates store Table12_4
* Collateral/ Loan

local loanchar_coll=" riskweight RLoanrate ZeroLoanrate maturity_m l_r_principal fx_dum"


capture drop charitable Ramadan_charitable
qui: g charitable = SECUREDfrac
qui: g Ramadan_charitable  = Ramadan * charitable

qui: reghdfe NPL2 Ramadan Ramadan_charitable charitable   $Macrovars `loanchar_coll' i.rating if $smallbiz, absorb(openingmonth openingyear activity bankbranch_fe ) vce(cluster  openingym) keepsing
estimates store Table12_5

drop charitable Ramadan_charitable
	
estfe Table12_*, labels(openingyear "Year FEs" openingmonth "Month FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs")

esttab Table12*, cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(25) label nomtitles level(95)  collabels(none) nodepvars keep(Ramadan Ramadan_charitable charitable ) ///
indicate("Borrower/loan characteristics =   $loanchar" "Macro control =   $Macrovars" `r(indicate_fe)') ///
stats(N r2, fmt(0 2) labels(`"N"' `"R-squared"')) title ("Table XII. Alternative Mechanisms") ///
mgroups("\# borrowers" "Small city" "First-time loan" "Risk rating" "Collateral to loan", pattern( 1 1 1 1 1)) ///
addnotes("Standard errors that are heteroskedasticity-consistent and clustered at the month-year level are reported in parentheses beneath coefficient estimates.")


end






*************************************************************************************
* Table XIII. Robustness
*************************************************************************************
capture program drop Table13
program define Table13


*Adha
qui: reghdfe NPL2 adha_loan  $Macrovars $loanchar i.rating if $smallbiz, absorb(openingmonth openingyear activity bankbranch_fe ) vce(cluster  openingym) keepsing
estimates store Table13_1


* First 10, second 10 and last 10 days of Ramadan
qui: reghdfe NPL2 Ramadhanfirst10 Ramadhansecond10 Ramadhanthird10  $Macrovars $loanchar i.rating if $smallbiz, absorb(openingmonth openingyear activity bankbranch_fe ) vce(cluster  openingym) keepsing
estimates store Table13_2
	

*Loan size < 100K (2008 prices)
qui: reghdfe NPL2 Ramadan  $Macrovars $loanchar i.rating  if $smallbiz & r_principal<100, absorb(openingmonth openingyear activity bankbranch_fe ) vce(cluster  openingym) keepsing
estimates store Table13_3
	

*Exclude Istanbul loans
qui: reghdfe NPL2 Ramadan  $Macrovars $loanchar i.rating  if $smallbiz & modcity!=34, absorb(openingmonth openingyear activity bankbranch_fe ) vce(cluster  openingym) keepsing
estimates store Table13_4

*Exclude 2007 & 2008 loans
qui: reghdfe NPL2 Ramadan  $Macrovars $loanchar i.rating if $smallbiz & openingyear!=2007 & openingyear!=2008, absorb(openingmonth openingyear activity bankbranch_fe ) vce(cluster  openingym) keepsing
estimates store Table13_5


estfe Table13_*, labels( openingyear "Year FEs" openingmonth "Month FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs")

esttab Table13_* ,cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(18) label nomtitles level(95)  collabels(none) nodepvars  /// 
keep(adha_loan Ramadhanfirst10 Ramadhansecond10 Ramadhanthird10 Ramadan  ) ///
indicate("Borrower/loan characteristics =   $loanchar" "Macro control =   $Macrovars" `r(indicate_fe)') ///
stats(N r2, fmt(0 2) labels(`"N"' `"R-squared"')) title (" Table XIII. Robustness")  ///
mgroups("All loans" "All loans" "Size<100K" "Exc. IST" "Exc. crisis" , pattern(1 1 1 1 1)) ///
addnotes("Standard errors that are heteroskedasticity-consistent and clustered at the month-year level are reported in parentheses beneath coefficient estimates.")



end



*************************************************************************************
*************************************************************************************
* TABLES FOR THE INTERNET APPENDIX
*************************************************************************************
*************************************************************************************



*************************************************************************************
*Table IA.I. Robustness of the Main Results to Different Choices of Standard Error Clustering
*************************************************************************************


*Cluster at branch-level

capture program drop TableIA1_Clst_Branch
program define TableIA1_Clst_Branch


qui: reghdfe NPL2 Ramadan  $Macrovars  if $smallbiz , absorb(openingyear openingmonth ) vce(cluster bankbranch_fe) keepsing
est store TableIA1br_1

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(openingyear openingmonth activity ) vce(cluster bankbranch_fe) keepsing
est store TableIA1br_2

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating  if $smallbiz , absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster bankbranch_fe) keepsing
est store TableIA1br_3

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(openingyear              activity bankbranchmonth_fe ) vce(cluster bankbranch_fe) keepsing
est store TableIA1br_4

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(             openingmonth activity bankbranchyear_fe ) vce(cluster bankbranch_fe) keepsing
est store TableIA1br_5

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(openingyear openingmonth activity bankbranch_fe borrower_fe)  vce(cluster bankbranch_fe) keepsing
est store TableIA1br_6

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz & span==1, absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster bankbranch_fe) keepsing
est store TableIA1br_7

estfe TableIA1br_* , labels(openingyear "Year FEs" openingmonth "Month FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs" borrower_fe "Borrower FEs" bankbranchmonth_fe "Branch x Month FEs" bankbranchyear_fe "Branch x Year FEs"  bankbranchborrower_fe "Branch x Borrower FEs")

esttab TableIA1br_*, cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(10) label  nomtitles collabels(none) nodepvars keep(Ramadan) ///
indicate("Borrower/loan characteristics = $loanchar" "Macro control =   $Macrovars" `r(indicate_fe)') ///
stats(N r2, fmt(0 3) labels(`"N"' `"R-squared"')) ///
title ("Table IA.I. Robustness of the Main Results to Different Choices of Standard Error Clustering (Clustered at Branch )") ///
addnotes("Standard errors that are heteroskedasticity-consistent and clustered at the bank branch level are reported in parentheses beneath coefficient estimates.")


end



*Cluster at lunar year-month -level

capture program drop TableIA1_Clst_lunarym
program define TableIA1_Clst_lunarym


qui: reghdfe NPL2 Ramadan  $Macrovars  if $smallbiz , absorb(openingyear openingmonth ) vce(cluster lunarym) keepsing
est store TableIA1lnym_1

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(openingyear openingmonth activity ) vce(cluster lunarym) keepsing
est store TableIA1lnym_2

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating  if $smallbiz , absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster lunarym) keepsing
est store TableIA1lnym_3

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(openingyear              activity bankbranchmonth_fe ) vce(cluster lunarym) keepsing
est store TableIA1lnym_4

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(             openingmonth activity bankbranchyear_fe ) vce(cluster lunarym) keepsing
est store TableIA1lnym_5

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(openingyear openingmonth activity bankbranch_fe borrower_fe)  vce(cluster lunarym) keepsing
est store TableIA1lnym_6

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz & span==1, absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster lunarym) keepsing
est store TableIA1lnym_7

estfe TableIA1lnym_* , labels(openingyear "Year FEs" openingmonth "Month FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs" borrower_fe "Borrower FEs" bankbranchmonth_fe "Branch x Month FEs" bankbranchyear_fe "Branch x Year FEs"  bankbranchborrower_fe "Branch x Borrower FEs")

esttab TableIA1lnym_*, cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(10) label  nomtitles collabels(none) nodepvars keep(Ramadan) ///
indicate("Borrower/loan characteristics = $loanchar" "Macro control =   $Macrovars" `r(indicate_fe)') ///
stats(N r2, fmt(0 3) labels(`"N"' `"R-squared"')) ///
title ("Table IA.I. Robustness of the Main Results to Different Choices of Standard Error Clustering (Clustered at Lunar Year-month )") ///
addnotes("Standard errors that are heteroskedasticity-consistent and clustered at the lunar month-year level are reported in parentheses beneath coefficient estimates.")

end



*Cluster at branch - lunar month level

capture program drop TableIA1_Clst_Brlunarm
program define TableIA1_Clst_Brlunarm


qui: reghdfe NPL2 Ramadan  $Macrovars  if $smallbiz , absorb(openingyear openingmonth ) vce(cluster branchlunarm) keepsing
est store TableIA1brlnm_1

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(openingyear openingmonth activity ) vce(cluster branchlunarm) keepsing
est store TableIA1brlnm_2

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating  if $smallbiz , absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster branchlunarm) keepsing
est store TableIA1brlnm_3

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(openingyear              activity bankbranchmonth_fe ) vce(cluster branchlunarm) keepsing
est store TableIA1brlnm_4

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(             openingmonth activity bankbranchyear_fe ) vce(cluster branchlunarm) keepsing
est store TableIA1brlnm_5

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(openingyear openingmonth activity bankbranch_fe borrower_fe)  vce(cluster branchlunarm) keepsing
est store TableIA1brlnm_6

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz & span==1, absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster branchlunarm) keepsing
est store TableIA1brlnm_7

estfe TableIA1brlnm_* , labels(openingyear "Year FEs" openingmonth "Month FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs" borrower_fe "Borrower FEs" bankbranchmonth_fe "Branch x Month FEs" bankbranchyear_fe "Branch x Year FEs"  bankbranchborrower_fe "Branch x Borrower FEs")

esttab TableIA1brlnm_*, cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(10) label  nomtitles collabels(none) nodepvars keep(Ramadan) ///
indicate("Borrower/loan characteristics = $loanchar" "Macro control =   $Macrovars" `r(indicate_fe)') ///
stats(N r2, fmt(0 3) labels(`"N"' `"R-squared"')) ///
title ("Table IA.I. Robustness of the Main Results to Different Choices of Standard Error Clustering (Clustered at Branch - Lunar Month )") ///
addnotes("Standard errors that are heteroskedasticity-consistent and clustered at the lunar month-year-bank branch level are reported in parentheses beneath coefficient estimates.")

end




*Cluster at branch and lunar month level

capture program drop TableIA1_Clst_Brandlunarm
program define TableIA1_Clst_Brandlunarm


qui: reghdfe NPL2 Ramadan  $Macrovars  if $smallbiz , absorb(openingyear openingmonth ) vce(cluster bankbranch_fe lunarmonth) keepsing
est store TableIA1brlnm_1

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(openingyear openingmonth activity ) vce(cluster bankbranch_fe lunarmonth) keepsing
est store TableIA1brlnm_2

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating  if $smallbiz , absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster bankbranch_fe lunarmonth) keepsing
est store TableIA1brlnm_3

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(openingyear              activity bankbranchmonth_fe ) vce(cluster bankbranch_fe lunarmonth) keepsing
est store TableIA1brlnm_4

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(             openingmonth activity bankbranchyear_fe ) vce(cluster bankbranch_fe lunarmonth) keepsing
est store TableIA1brlnm_5

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(openingyear openingmonth activity bankbranch_fe borrower_fe)  vce(cluster bankbranch_fe lunarmonth) keepsing
est store TableIA1brlnm_6

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz & span==1, absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster bankbranch_fe lunarmonth) keepsing
est store TableIA1brlnm_7

estfe TableIA1brlnm_* , labels(openingyear "Year FEs" openingmonth "Month FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs" borrower_fe "Borrower FEs" bankbranchmonth_fe "Branch x Month FEs" bankbranchyear_fe "Branch x Year FEs"  bankbranchborrower_fe "Branch x Borrower FEs")

esttab TableIA1brlnm_*, cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(10) label  nomtitles collabels(none) nodepvars keep(Ramadan) ///
indicate("Borrower/loan characteristics = $loanchar" "Macro control =   $Macrovars" `r(indicate_fe)') ///
stats(N r2, fmt(0 3) labels(`"N"' `"R-squared"')) ///
title ("Table IA.I. Robustness of the Main Results to Different Choices of Standard Error Clustering (Clustered at Branch and Lunar Month )") ///
addnotes("Standard errors that are heteroskedasticity-consistent and double clustered at the lunar month and bank branch level are reported in parentheses beneath coefficient estimates.")

end


*Cluster at province level

capture program drop TableIA1_Clst_province
program define TableIA1_Clst_province


qui: reghdfe NPL2 Ramadan  $Macrovars  if $smallbiz , absorb(openingyear openingmonth ) vce(cluster modcity) keepsing
est store TableIA1prv_1

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(openingyear openingmonth activity ) vce(cluster modcity) keepsing
est store TableIA1prv_2

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating  if $smallbiz , absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster modcity) keepsing
est store TableIA1prv_3

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(openingyear              activity bankbranchmonth_fe ) vce(cluster modcity) keepsing
est store TableIA1prv_4

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(             openingmonth activity bankbranchyear_fe ) vce(cluster modcity) keepsing
est store TableIA1prv_5

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(openingyear openingmonth activity bankbranch_fe borrower_fe)  vce(cluster modcity) keepsing
est store TableIA1prv_6

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz & span==1, absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster modcity) keepsing
est store TableIA1prv_7

estfe TableIA1prv_* , labels(openingyear "Year FEs" openingmonth "Month FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs" borrower_fe "Borrower FEs" bankbranchmonth_fe "Branch x Month FEs" bankbranchyear_fe "Branch x Year FEs"  bankbranchborrower_fe "Branch x Borrower FEs")

esttab TableIA1prv_*, cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(10) label  nomtitles collabels(none) nodepvars keep(Ramadan) ///
indicate("Borrower/loan characteristics = $loanchar" "Macro control =   $Macrovars" `r(indicate_fe)') ///
stats(N r2, fmt(0 3) labels(`"N"' `"R-squared"')) ///
title ("Table IA.I. Robustness of the Main Results to Different Choices of Standard Error Clustering (Clustered at Province )") ///
addnotes("Standard errors that are heteroskedasticity-consistent and clustered at the province level are reported in parentheses beneath coefficient estimates.")


end



*Cluster at day of year level

capture program drop TableIA1_Clst_doy
program define TableIA1_Clst_doy


qui: reghdfe NPL2 Ramadan  $Macrovars  if $smallbiz , absorb(openingyear openingmonth ) vce(cluster doy) keepsing
est store TableIA1doy_1

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(openingyear openingmonth activity ) vce(cluster doy) keepsing
est store TableIA1doy_2

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating  if $smallbiz , absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster doy) keepsing
est store TableIA1doy_3

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(openingyear              activity bankbranchmonth_fe ) vce(cluster doy) keepsing
est store TableIA1doy_4

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(             openingmonth activity bankbranchyear_fe ) vce(cluster doy) keepsing
est store TableIA1doy_5

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(openingyear openingmonth activity bankbranch_fe borrower_fe)  vce(cluster doy) keepsing
est store TableIA1doy_6

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz & span==1, absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster doy) keepsing
est store TableIA1doy_7

estfe TableIA1doy_* , labels(openingyear "Year FEs" openingmonth "Month FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs" borrower_fe "Borrower FEs" bankbranchmonth_fe "Branch x Month FEs" bankbranchyear_fe "Branch x Year FEs"  bankbranchborrower_fe "Branch x Borrower FEs")

esttab TableIA1doy_*, cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(10) label  nomtitles collabels(none) nodepvars keep(Ramadan) ///
indicate("Borrower/loan characteristics = $loanchar" "Macro control =   $Macrovars" `r(indicate_fe)') ///
stats(N r2, fmt(0 3) labels(`"N"' `"R-squared"')) ///
title ("Table IA.I. Robustness of the Main Results to Different Choices of Standard Error Clustering (Clustered at the Day of Year )") ///
addnotes("Standard errors that are heteroskedasticity-consistent and clustered at the day of year level are reported in parentheses beneath coefficient estimates.")

end



*Cluster at 10-day intervals

capture program drop TableIA1_Clst_Doy10
program define TableIA1_Clst_Doy10


qui: reghdfe NPL2 Ramadan  $Macrovars  if $smallbiz , absorb(openingyear openingmonth ) vce(cluster Doy10) keepsing
est store TableIA1doy10_1

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(openingyear openingmonth activity ) vce(cluster Doy10) keepsing
est store TableIA1doy10_2

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating  if $smallbiz , absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster Doy10) keepsing
est store TableIA1doy10_3

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(openingyear              activity bankbranchmonth_fe ) vce(cluster Doy10) keepsing
est store TableIA1doy10_4

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(             openingmonth activity bankbranchyear_fe ) vce(cluster Doy10) keepsing
est store TableIA1doy10_5

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz , absorb(openingyear openingmonth activity bankbranch_fe borrower_fe)  vce(cluster Doy10) keepsing
est store TableIA1doy10_6

qui: reghdfe NPL2 Ramadan $loanchar  $Macrovars i.rating if $smallbiz & span==1, absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster Doy10) keepsing
est store TableIA1doy10_7

estfe TableIA1doy10_* , labels(openingyear "Year FEs" openingmonth "Month FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs" borrower_fe "Borrower FEs" bankbranchmonth_fe "Branch x Month FEs" bankbranchyear_fe "Branch x Year FEs"  bankbranchborrower_fe "Branch x Borrower FEs")

esttab TableIA1doy10_*, cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(10) label  nomtitles collabels(none) nodepvars keep(Ramadan) ///
indicate("Borrower/loan characteristics = $loanchar" "Macro control =   $Macrovars" `r(indicate_fe)') ///
stats(N r2, fmt(0 3) labels(`"N"' `"R-squared"')) ///
title ("Table IA.I. Robustness of the Main Results to Different Choices of Standard Error Clustering (Clustered at 10-Day Intervals )") ///
addnotes("Standard errors that are heteroskedasticity-consistent and clustered at the 10-day intervals level are reported in parentheses beneath coefficient estimates.")

end







*************************************************************************************
*Table IA.II. Robustness of the Main Results to the Exclusion of Macro Control
*************************************************************************************

capture program drop TableIA2
program define TableIA2


qui: reghdfe NPL2 Ramadan    if $smallbiz , absorb(openingyear openingmonth ) vce(cluster  openingym) keepsing
est store TableIA2_1

qui: reghdfe NPL2 Ramadan   $loanchar i.rating  if $smallbiz , absorb(openingyear openingmonth activity ) vce(cluster  openingym) keepsing
est store TableIA2_2

qui: reghdfe NPL2 Ramadan   $loanchar i.rating  if $smallbiz , absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster  openingym) keepsing
est store TableIA2_3

qui: reghdfe NPL2 Ramadan   $loanchar i.rating  if $smallbiz , absorb(openingyear              activity bankbranchmonth_fe ) vce(cluster  openingym) keepsing
est store TableIA2_4

qui: reghdfe NPL2 Ramadan   $loanchar i.rating  if $smallbiz , absorb(             openingmonth activity bankbranchyear_fe ) vce(cluster  openingym) keepsing
est store TableIA2_5

qui: reghdfe NPL2 Ramadan   $loanchar i.rating  if $smallbiz , absorb(openingyear openingmonth activity bankbranch_fe borrower_fe)  vce(cluster  openingym) keepsing
est store TableIA2_6

qui: reghdfe NPL2 Ramadan   $loanchar i.rating  if $smallbiz & span==1, absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster  openingym) keepsing
est store TableIA2_7

estfe TableIA2_*, labels(openingyear "Year FEs" openingmonth "Month FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs" borrower_fe "Borrower FEs" bankbranchmonth_fe "Branch x Month FEs" bankbranchyear_fe "Branch x Year FEs"  bankbranchborrower_fe "Branch x Borrower FEs")

esttab TableIA2_* ,cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(10)  nomtitles level(95)  collabels(none) nodepvars keep(Ramadan ) ///
indicate("Borrower/loan characteristics =   $loanchar" "Macro control =   " `r(indicate_fe)') ///
stats(N r2, fmt(0 3) labels(`"N"' `"R-squared"')) ///
title ("Table IA.II. Robustness of the Main Results to the Exclusion of Macro Control") ///
addnotes("Standard errors that are heteroskedasticity-consistent and clustered at the month-year level are reported in parentheses beneath coefficient estimates.")

end


*************************************************************************************
*Table IA.III. Robustness of the Main Results to the Exclusion of Borrower and Loan Characteristics
*************************************************************************************

capture program drop TableIA3
program define TableIA3


qui: reghdfe NPL2 Ramadan $Macrovars   if $smallbiz , absorb(openingyear openingmonth ) vce(cluster  openingym) keepsing
est store TableIA3_1

qui: reghdfe NPL2 Ramadan $Macrovars     if $smallbiz , absorb(openingyear openingmonth activity ) vce(cluster  openingym) keepsing
est store TableIA3_2

qui: reghdfe NPL2 Ramadan $Macrovars     if $smallbiz , absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster  openingym) keepsing
est store TableIA3_3

qui: reghdfe NPL2 Ramadan $Macrovars    if $smallbiz , absorb(openingyear              activity bankbranchmonth_fe ) vce(cluster  openingym) keepsing
est store TableIA3_4

qui: reghdfe NPL2 Ramadan $Macrovars    if $smallbiz , absorb(             openingmonth activity bankbranchyear_fe ) vce(cluster  openingym) keepsing
est store TableIA3_5

qui: reghdfe NPL2 Ramadan $Macrovars    if $smallbiz , absorb(openingyear openingmonth activity bankbranch_fe borrower_fe)  vce(cluster  openingym) keepsing
est store TableIA3_6

qui: reghdfe NPL2 Ramadan $Macrovars    if $smallbiz & span==1, absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster  openingym) keepsing
est store TableIA3_7

estfe TableIA3_*, labels(openingyear "Year FEs" openingmonth "Month FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs" borrower_fe "Borrower FEs" bankbranchmonth_fe "Branch x Month FEs" bankbranchyear_fe "Branch x Year FEs"  bankbranchborrower_fe "Branch x Borrower FEs")

esttab TableIA3_* ,cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(10)  nomtitles level(95)  collabels(none) nodepvars  keep(Ramadan ) ///
indicate( "Macro control =   $Macrovars" "Borrower/loan characteristics = " `r(indicate_fe)') ///
stats(N r2, fmt(0 3) labels(`"N"' `"R-squared"')) ///
title ("Table IA.III. Robustness of the Main Results to the Exclusion of Borrower and Loan Characteristics") ///
addnotes("Standard errors that are heteroskedasticity-consistent and clustered at the month-year level are reported in parentheses beneath coefficient estimates.")


end 



*************************************************************************************
*Table IA.IV. Yearly Default Models That Exclude the Macro Control
*************************************************************************************

capture program drop TableIA4
program define TableIA4

*No VIX
foreach y of numlist 2003/2013{
qui:reghdfe NPL2 Ramadan  $loanchar i.rating if  openingyear==`y' & $smallbiz ,abs( bankbranch_fe activity) vce(robust) 
est store TableIA4_`y'
}



estfe TableIA4_*, labels(openingyear "Year FEs" openingmonth "Month FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs" borrower_fe "Borrower FEs" bankbranchmonth_fe "Branch x Month FEs" bankbranchyear_fe "Branch x Year FEs"  bankbranchborrower_fe "Branch x Borrower FEs")

esttab TableIA4_* , cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(10)  nomtitles level(95)  collabels(none) nodepvars  keep(Ramadan ) ///
indicate( "Macro control =  " "Borrower/loan characteristics = $loanchar" `r(indicate_fe)') ///
stats(N r2, fmt(0 3) labels(`"N"' `"R-squared"')) ///
title ("Table IA.IV. Yearly Default Models That Exclude the Macro Control") ///
mgroups("2003" "2004" "2005" "2006" "2007" "2008" "2009" "2010" "2011" "2012" "2013" , pattern(1 1 1 1 1 1 1 1 1 1 1)) ///
addnotes("Standard errors that are heteroskedasticity-consistent are reported in parentheses beneath coefficient estimates.")

end


*************************************************************************************
*Table IA.V. Yearly Default Models with Loans Originated Within a Short Time Window Around Ramadan
*************************************************************************************


capture program drop TableIA5
program define TableIA5


foreach m of numlist 2003/2013{

qui:reghdfe NPL2 Ramadan  maturity_m l_r_principal fx_dum SECUREDfrac Lvix_eop RLoanrate ZeroLoanrate i.rating riskweight if  openingyear==`m' & ///
(Ramadan==1 | Second10BDb==1 | First10BDb==1 |  First10BDafterramadan==1 |  Second10BDafterramadan==1) & $smallbiz ,abs( bankbranch_fe activity) vce(robust) 
est store TableIA5_`m'

}

estfe TableIA5_*, labels(openingyear "Year FEs" openingmonth "Month FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs" borrower_fe "Borrower FEs" bankbranchmonth_fe "Branch x Month FEs" bankbranchyear_fe "Branch x Year FEs"  bankbranchborrower_fe "Branch x Borrower FEs")

esttab TableIA5_* , cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(10)  nomtitles level(95)  collabels(none) nodepvars  keep(Ramadan ) ///
indicate( "Macro control = Lvix_eop " "Borrower/loan characteristics = $loanchar" `r(indicate_fe)') ///
stats(N r2, fmt(0 3) labels(`"N"' `"R-squared"')) ///
title ("Table IA.V. Yearly Default Models with Loans Originated Within a Short Time Window Around Ramadan") ///
mgroups("2003" "2004" "2005" "2006" "2007" "2008" "2009" "2010" "2011" "2012" "2013" , pattern(1 1 1 1 1 1 1 1 1 1 1)) 


end



*************************************************************************************
*Table IA.VI. The Effect of Ramadan on the Likelihood of Default by Calendar Month
*************************************************************************************

capture program drop TableIA6
program define TableIA6



*Add year FE
foreach m of numlist 7/11{
qui:reghdfe NPL2 Ramadan  maturity_m l_r_principal fx_dum SECUREDfrac Lvix_eop RLoanrate ZeroLoanrate i.rating riskweight if  openingmonth==`m' & $smallbiz ,abs(openingyear bankbranch_fe activity) vce(robust) 
est store TableIA6_`m'
}


estfe TableIA6_*, labels(openingyear "Year FEs" openingmonth "Month FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs" borrower_fe "Borrower FEs" bankbranchmonth_fe "Branch x Month FEs" bankbranchyear_fe "Branch x Year FEs"  bankbranchborrower_fe "Branch x Borrower FEs")

esttab TableIA6_* , cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(10)  nomtitles level(95)  collabels(none) nodepvars  keep(Ramadan ) ///
indicate( "Macro control = Lvix_eop " "Borrower/loan characteristics = $loanchar" `r(indicate_fe)') ///
stats(N r2, fmt(0 3) labels(`"N"' `"R-squared"')) ///
title ("Table IA.VI. The Effect of Ramadan on the Likelihood of Default by Calendar Month") ///
mgroups("July" "August" "September" "October" "November" , pattern(1 1 1 1 1 )) ///
addnotes("Standard errors that are heteroskedasticity-consistent  are reported in parentheses beneath coefficient estimates.")

end




*************************************************************************************
* Table IA.VII. The Effect of Ramadan by Loan Security
*************************************************************************************

capture program drop TableIA7
program define TableIA7

*label var unsecured "Unsecured Loan"

qui: reghdfe NPL2 Ramadan  $Macrovars $loanchar i.rating if $smallbiz & unsecured==1, absorb(openingyear openingmonth activity bankbranch_fe) vce(cluster openingym) keepsing
est store TableIA7_1

qui: reghdfe NPL2 Ramadan  $Macrovars $loanchar i.rating if $smallbiz & undersecured==1, absorb(openingyear openingmonth activity bankbranch_fe) vce(cluster openingym) keepsing
est store TableIA7_2

qui: reghdfe NPL2 Ramadan  $Macrovars $loanchar i.rating if $smallbiz & secured==1, absorb(openingyear openingmonth activity bankbranch_fe) vce(cluster openingym) keepsing
est store TableIA7_3


estfe TableIA7_*, labels(openingyear "Year FEs" openingmonth "Month FEs"  activity "Loan purpose FEs" bankbranch_fe "Branch FEs")

esttab TableIA7_*, cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(20) label nomtitles level(95)  collabels(none) nodepvars  /// 
indicate("Borrower/loan characteristics =   $loanchar" "Macro control =   $Macrovars" `r(indicate_fe)') ///
stats(N r2, fmt(0 2) labels(`"N"' `"R-squared"')) keep(Ramadan) ///
title ("Table IA.VII. The Effect of Ramadan by Loan Security") ///
mgroups("Unsecured" "Undersecured" "Secured" , pattern(1 1 1)) ///
addnotes("Standard errors that are heteroskedasticity-consistent and clustered at the month-year level are reported in parentheses beneath coefficient estimates.")

 
end 



****************************************************************
*Table IA.VIII. Hot/Cold and Summer/Non-Summer Sample Splits
****************************************************************

capture program drop TableIA8
program define TableIA8


qui:reghdfe NPL2 Ramadan $Macrovars $loanchar i.rating  if $smallbiz & Hotday==1 & span==1 & busyBr3==4 , absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster openingym) keepsing
est store TableIA8_1
qui:reghdfe NPL2 Ramadan $Macrovars $loanchar i.rating  if $smallbiz & Coldday==1 & span==1 & busyBr3==4, absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster openingym) keepsing
est store TableIA8_2
qui:reghdfe NPL2 Ramadan $loanchar i.rating $Macrovars  if $smallbiz & summer==1 & span==1 & busyBr3==4, absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster openingym) keepsing
est store TableIA8_3
qui:reghdfe NPL2 Ramadan $loanchar i.rating $Macrovars  if $smallbiz & summer==0 & span==1 & busyBr3==4, absorb(openingyear openingmonth activity bankbranch_fe ) vce(cluster openingym) keepsing
est store TableIA8_4

estfe TableIA8_* , labels(openingyear "Year FEs" openingmonth "Month FEs" activity "Loan purpose FEs" bankbranch_fe "Branch FEs" borrower_fe "Borrower FEs" bankbranchmonth_fe "Branch x Month FEs" bankbranchyear_fe "Branch x Year FEs"  bankbranchborrower_fe "Branch x Borrower FEs")

esttab TableIA8_* ,cells(b(star fmt(3)) se(fmt(3) par abs) )  star(* 0.10 ** 0.05 *** 0.01)  ///
varwidth(30) modelwidth(10) label  nomtitles collabels(none) nodepvars  ///
indicate("Borrower/loan characteristics = $loanchar" "Macro control = $Macrovars"  `r(indicate_fe)') ///
stats(N r2, fmt(0 3) labels(`"N"' `"R-squared"')) keep(Ramadan) ///
title ("Table IA.VIII. Hot/Cold and Summer/Non-Summer Sample Splits ") ///
mgroups("Hot" "Cold" "Summer" "Non-Summer", pattern(1 1 1 1)) ///
addnotes("Standard errors that are heteroskedasticity-consistent and clustered at the month-year level are reported in parentheses beneath coefficient estimates.")


end









*****************************
* Run regressions in modules
*****************************


capture log close
log using "$dir_log/TablesFinal", text replace

use "$dir_Ddta/ForRamadan_Apr20.dta", clear

Table1
Table2
Table3
Table4
Table5
Table6
Table7
Table9
Table10
Table11
Table12
Table13

TableIA1_Clst_Branch
TableIA1_Clst_lunarym
TableIA1_Clst_Brlunarm
TableIA1_Clst_Brandlunarm
TableIA1_Clst_province
TableIA1_Clst_doy
TableIA1_Clst_Doy10

TableIA2
TableIA3
TableIA4
TableIA5
TableIA6
TableIA7
TableIA8


use "$dir_Ddta/RamadanNoofData", clear

Table8

log close

```